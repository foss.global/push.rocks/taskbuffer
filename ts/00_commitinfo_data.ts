/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/taskbuffer',
  version: '3.0.10',
  description: 'flexible task management. TypeScript ready!'
}
