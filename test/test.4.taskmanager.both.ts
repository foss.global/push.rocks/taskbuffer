import { expect, tap } from '@pushrocks/tapbundle';

import * as taskbuffer from '../ts/index.js';

import * as smartpromise from '@pushrocks/smartpromise';
import * as smartdelay from '@pushrocks/smartdelay';

let myTaskManager: taskbuffer.TaskManager;
let taskRunCounter = 0;
const taskDone = smartpromise.defer();

tap.test('should create an instance of TaskManager', async () => {
  myTaskManager = new taskbuffer.TaskManager();
  expect(myTaskManager).toBeInstanceOf(taskbuffer.TaskManager);
});

tap.test('should run the task as expected', async () => {
  let referenceBoolean = false;
  myTaskManager.addTask(
    new taskbuffer.Task({
      name: 'myTask',
      bufferMax: 1,
      buffered: true,
      taskFunction: async () => {
        console.log('Task "myTask" executed!');
        referenceBoolean = true;
        taskRunCounter++;
        if (taskRunCounter === 10) {
          taskDone.resolve();
        }
      },
    })
  );
  myTaskManager.start();
  await myTaskManager.triggerTaskByName('myTask');
  // tslint:disable-next-line:no-unused-expression
  expect(referenceBoolean).toBeTrue();
});

tap.test('should schedule task', async () => {
  myTaskManager.scheduleTaskByName('myTask', '*/10 * * * * *');
  await taskDone.promise;
  myTaskManager.descheduleTaskByName('myTask');
});

tap.test('should stop the taskmanager', async () => {
  myTaskManager.stop();
});

tap.start();
